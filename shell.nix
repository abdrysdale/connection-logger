with (import <nixpkgs> {});
let
  py-packages = python-packages: with python-packages; [
    numpy
    pandas
    matplotlib
  ];
  full-py-packages = python3.withPackages py-packages;
in
  mkShell {
    buildInputs = [
      full-py-packages
      pkgs.bash
      pkgs.speedtest-cli
      pkgs.dateutils
      pkgs.util-linux
      pkgs.unixtools.ping
    ];
  }
