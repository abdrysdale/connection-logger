# Connection Logger

This is a simple script to continuously log the internet connection.
It is intended to be run on a Raspberry Pi and used to hold the internet service provider accountable to speed claims.

## Aims:
- [x] Continuously log internet connection.
- [ ] Show monthly connectivity in an easy to visualise manner.
- [ ] Show percentage of the month speed claims where not met.
- [ ] Show expected refund amount (if any).
- [ ] Draft an email to ISP and send to user.
- [ ] Easily used with a docker container and instructions.

## Installation

- Clone the repo.
- Edit variable file.
- Build the docker container.
- Run the container.

## Methods

### Continuously log connection

The bash script continuously runs the following and logs the result to a csv file:

- Pings hub and gets package loss.
- Pings external web service and gets package loss.
- Speedtest and gets upload, download and ping time.

Along with logging the date.

## Validating speed claims

The python script analyses the csv file in the following manner:

- Plots upload as a function of time - highlighting areas below speed claims.
- Plots download as a function of time - highlighting areas below speed claims.
- Calculates percentage of time spent below speed claims - through integration.
- Calculates expected refund amount - from percentage of time spent below claims.
- Sends a summary email to the user and, if a refund is expected, sends a draft email for a claim.
