#! /usr/bin/env bash

tmp_file="/tmp/log_connection.out"
csv_file="connection_log.csv"
hub_ip="192.168.178.1"
ext_ip="www.google.com"
fields="Time,Hub ping,External ping,Ping (ms),Download (Mb/s),Upload (Mb/s)"

if [ ! -f ${csv_file} ]
then
	echo ${fields} > ${csv_file}
fi

while true
do
	# Gets the speed
	speedtest --simple | awk '{print $2}' > ${tmp_file}
	ping=$(cat ${tmp_file} | head -n1)
	download=$(cat ${tmp_file} | head -n2 | tail -n1)
	upload=$(cat ${tmp_file} | tail -n1)

	# Gets pings
	hub_ip_stats=$(ping -c1 ${hub_ip} | grep "packet loss" | rev | cut -d"," -f2 | rev)
	ext_ip_stats=$(ping -c1 ${ext_ip} | grep "packet loss" | rev | cut -d"," -f2 | rev)

	# Gets time
	time=$(date "+%F %R")


	echo "${time},${hub_ip_stats},${ext_ip_stats},${ping},${download},${upload}" >> ${csv_file}
done
